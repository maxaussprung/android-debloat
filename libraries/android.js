const { ADB } = require('appium-adb');
const fs = require('fs');

async function rm(apps) {
    const adb = await ADB.createADB();
    for (let a of apps)
        try {
            await adb.shell(`pm uninstall -k --user 0 ${a}`);
        }
        catch (err) {
            throw err;
        }
}

function loadConfig(path) {
    return fs.readFileSync('mimix2.txt', 'UTF8').split('\r\n\r\n');
}

async function localApps() {
    const adb = await ADB.createADB();

    try {
        const apps = await adb.shell(`pm list packages`);
        return apps.replace(/package:/g,'').split('\n');
    }
    catch (err) {
        throw err;
    }
}

async function devices(){
    const adb = await ADB.createADB();
    return await adb.isDeviceConnected();
}

module.exports = {
    loadConfig, localApps, rm, devices
}
