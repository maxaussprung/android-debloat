const android = require('./libraries/android');

const app = new Vue({
  el: 'main',
  data: {
    apps: [],
    selectedApps: [],
    ready: false,
    search: '',
    popup: false
  },
  methods: {
    async localApps() {
      this.apps = await android.localApps();
      this.apps.sort();
    },
    async deleteApps() {
      await android.rm(this.selectedApps);
      this.search = '';
      this.selectedApps = [];
      this.init();
    },
    async init() {
      while(this.ready == false){
        this.ready = await android.devices();
      }
      this.apps = await android.localApps();
      this.apps.sort();
    },
    openpopup() {
      document.querySelector('#modal').classList.toggle('show');
    }
  },
  computed: {
    filterApps() {
      return this.apps.filter(app => {
        console.log(this.search);
        return app.toLowerCase().includes(this.search.toLowerCase())
      });
    }
  },
  created() {
    this.init();
  }
})